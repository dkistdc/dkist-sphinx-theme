# DKIST Sphinx Theme

The DKIST documentation theme

In your sphinx `conf.py` file you will need to put the following:

```python
html_theme = "dkist"
```

This theme is based on the [sunpy theme](https://github.com/sunpy/sunpy-sphinx-theme).
